﻿ using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
	}

	void OnCollisionEnter(Collision collision) {
		Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
