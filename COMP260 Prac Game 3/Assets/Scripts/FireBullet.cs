﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	public float timer;

	// Use this for initialization
	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown ("Fire1")) {

			BulletMove bullet = Instantiate (bulletPrefab);
			bullet.transform.position = transform.position;

			Ray ray =
				Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
		}
		timer += 1.0f * Time.deltaTime;
		if (timer >= 4) {
			BulletMove.Destroy (bulletPrefab);
		}
		if (Time.timeScale == 0) {
			return;
		}
			
	}

}



